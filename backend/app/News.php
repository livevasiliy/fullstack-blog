<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    protected $fillable = ['title', 'body', 'userId'];
    
    public function user ()
	{
		return $this->belongsTo (User::class, 'userId');
	}
}
