<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewsRequest;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return News[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return News::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateNewsRequest $request
	 * @return \Illuminate\Http\Response
	 */
    public function store(CreateNewsRequest $request)
    {
        $news = News::create([
            'title' => $request->get('title'),
			'body' => $request->get ('body'),
			'userId' => Auth::user()->id
		]);
        if (!news){
        	return redirect ()->back();
		}
		response ()->json ($news);
        return redirect()->route('admin-panel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::findOrFail($id);
        return response ()->json ($news);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        return response ()->json ($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$news = News::findOrFail ($id);
		$news->fill ($request->all ());
	
		if (!$news->save ())
		{
			return redirect ()->back();
		}
		return redirect ()->route('admin-panel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        if (!$news->delete())
		{
			return redirect ()->back ()->withErrors ('Delete error');
		}
		return redirect ()->back ();
    }
}
