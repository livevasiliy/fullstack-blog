import Vue from 'vue'
import Vuex from 'vuex'
import news from './news'
import users from './users'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    news, users
  }
})
