import * as axios from 'axios'

export default {
  state: {
    articles: []
  },
  getters: {
    async getAllNews (state) {
      return state.articles
    }
  },
  actions: {
    async fetchNews ({commit}) {
      const result = []
      try {
        const data = await axios({
          method: 'get',
          url: 'http://localhost:8000/api/news/',
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
          }
        }).then(response => {
          return response
        })
        result.push(data)
        commit('loadArticles', result)
      } catch (error) {
        console.log(error.message)
        throw error
      }
    }
  },
  mutations: {
    loadArticles (state, payload) {
      state.articles = payload
    }
  }
}
